﻿using System;
using System.IO;

namespace HideZipInImage
{
    class Program
    {

        public static String sourceImage = "image.jpg";
        public static String sourceArchive = "archive.zip";
        public static String outputImage = "output.jpg";

        static void Main(string[] args)
        {
            Console.Title = "HIDE ZIP IN IMAGE v1.0";
            Console.WriteLine("[ HIDE ZIP IN IMAGE v1.0 ]");

            if(!File.Exists(sourceImage))
            {
                Console.Write("Source image not found: "+sourceImage+"\r\n");
                Console.ReadKey();
                return;
            }

            if (!File.Exists(sourceArchive))
            {
                Console.Write("Source archive not found: "+sourceArchive+"\r\n");
                Console.ReadKey();
                return;
            }

            // read bytes
            byte[] image = File.ReadAllBytes(sourceImage);
            byte[] archive = File.ReadAllBytes(sourceArchive);

            // create array
            byte[] compiled = new byte[image.Length + archive.Length];

            // add image bytes
            for (int i = 0; i < image.Length; i++)
            {
                compiled[i] = image[i];
            }

            // add archive bytes
            for (int i = 0; i < archive.Length; i++)
            {
                compiled[image.Length+i] = archive[i];
            }

            File.WriteAllBytes(outputImage, compiled);
            Console.Write("Output image: " + outputImage + "\r\n");
            Console.ReadKey();

        }
    }
}
